#!/bin/bash

ENVIRONMENT="$1"
VERSION="$2"

if [ -z "$ENVIRONMENT" ]; then
  ENVIRONMENT="dev"
fi;

if [ ! -z "$VERSION" ]; then
  ARG_VERSION="--version $VERSION"
fi;

helm upgrade dumpenv-$ENVIRONMENT chartmuseum/demochart --values ./helmcharts/demochart/values-$ENVIRONMENT.yaml $ARG_VERSION