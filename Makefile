all: build

build:
	export VERSION=$$(cat VERSION); \
	echo $$VERSION; \
	export IMAGE_VERSIONED="hdkiller/dump-envs:$${VERSION}"; \
	export IMAGE_LATEST="hdkiller/dump-envs:latest"; \
	docker build -t $$IMAGE_VERSIONED .; \
	docker tag $$IMAGE_VERSIONED $$IMAGE_LATEST


publish:
	docker push hdkiller/dump-envs

run:
	docker run -p 8888:8888 --name dump-envs hdkiller/dump-envs

test:
	helm lint helmcharts/demochart

package:
	cd helmcharts/demochart && helm package .
