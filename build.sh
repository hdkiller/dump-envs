#!/bin/bash

# bump chart version
helm local-chart-version bump -s patch -c ./helmcharts/demochart/

# bump application version
pybump bump --app-version --level=patch --file ./helmcharts/demochart/Chart.yaml
pybump bump --app-version --level=patch --file ./apps/dump-envs/VERSION

# app version
VERSION="$(cat ./apps/dump-envs/VERSION)"

IMAGE_VERSIONED="192.168.0.25:5000/hdkiller/dump-envs:${VERSION}"
IMAGE_LATEST="192.168.0.25:5000/hdkiller/dump-envs:latest"

docker build -t $IMAGE_VERSIONED apps/dump-envs
docker tag $IMAGE_VERSIONED $IMAGE_LATEST
docker push $IMAGE_VERSIONED
docker push $IMAGE_LATEST

# export DOCKER_VERSION=$(cat VERSION)
# export CHART_VERSION=$(cat helmcharts/demochart/Chart.yaml | grep -m 1 ^version:| awk '{print $2}')

# GIT_BRANCH="$(git name-rev --name-only HEAD)"
#helm push helmcharts/demochart --version="${GIT_BRANCH}-${VERSION}-$(git log -1 --pretty=format:%h)" chartmuseum

helm push helmcharts/demochart chartmuseum
helm repo update
helm search repo --versions chartmuseum/demochart

