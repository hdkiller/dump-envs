'use strict';

let express = require('express');
let app = express();

const PORT = process.env.PORT || 8888;

console.log('Server listening on %d', PORT);

app.get("/", function(req,response) {
    response.json(process.env);
});

app.listen(PORT);
